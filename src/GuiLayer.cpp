#include <iostream>

#include <eco-graphics/GuiLayer.hpp>
#include <eco-graphics/RenderWindow.hpp>

#include <glm/gtc/matrix_transform.hpp>

namespace ecosystems::graphics {

    void GuiLayer::Init(Device& p_device) {
        for(auto& component : m_ui_components) {
            component->Init(p_device);
        }
    }

    void GuiLayer::Update(float p_delta_time) {
        for(auto& component : m_ui_components) {
            component->Update(p_delta_time);
        }
    }

    void GuiLayer::Render(VkCommandBuffer& p_command_buffer) {
        for(auto& component : m_ui_components) {
            // TODO: Render
        }
    }


    /* * * * * * * * * * * * * *
     *   GLFW INPUT HANDLERS   *
     * * * * * * * * * * * * * */
    void GuiLayer::glfw_error_callback(int p_error, const char* p_description) {
        for(auto& component : m_ui_components) {
            component->on_glfw_error(p_error, p_description);
        }
    }

    void GuiLayer::glfw_keypress_callback(GLFWwindow *p_window, int p_key, int p_scancode, int p_action, int p_mods) {
        for(auto& component : m_ui_components) {
            if(p_action == 1 || p_action == 2) {
                component->on_key_down(p_key, p_scancode, p_mods);
            } else if (p_action == 0) {
                component->on_key_up(p_key, p_scancode, p_mods);
            }
        }
    }

    void GuiLayer::glfw_cursor_position_callback(GLFWwindow *p_window, double p_xpos, double p_ypos) {
        auto renderWindow = reinterpret_cast<RenderWindow*>(glfwGetWindowUserPointer(p_window));
        glm::dvec2 mousePosition{p_xpos, p_ypos};
        glfwGetCursorPos(p_window, &mousePosition.x, &mousePosition.y);

        // invert mouse y cuz vulkan is backwards or something like that
        mousePosition.y = -(mousePosition.y - renderWindow->get_extent().height);

        for(auto& component : m_ui_components) {
            component->on_mouse_move(mousePosition);
        }
    }

    void GuiLayer::glfw_mouse_button_callback(GLFWwindow *p_window, int p_button, int p_action, int p_mods) {
        auto renderWindow = reinterpret_cast<RenderWindow*>(glfwGetWindowUserPointer(p_window));
        glm::dvec2 mousePosition(0, 0);
        glfwGetCursorPos(p_window, &mousePosition.x, &mousePosition.y);

        // invert mouse y cuz vulkan is backwards or something like that
        mousePosition.y = -(mousePosition.y - renderWindow->get_extent().height);

        for(auto& component : m_ui_components) {
            if(p_action == GLFW_PRESS) {
                component->on_mouse_button_down(p_button, p_mods, mousePosition);
            } else if(p_action == GLFW_RELEASE) {
                component->on_mouse_button_up(p_button, p_mods, mousePosition);
            }
        }

    }

    void GuiLayer::glfw_frame_buffer_size_callback(GLFWwindow *window, int p_width, int p_height) {
        for(auto& component : m_ui_components) {
            component->on_frame_buffer_resize(p_width, p_height);
        }
    }

    void GuiLayer::glfw_character_mods_callback(GLFWwindow *p_window, unsigned int p_code_point, int p_mods) {
        for(auto& component : m_ui_components) {
            component->on_char_input(p_code_point, p_mods);
        }
    }

    void GuiLayer::glfw_character_callback(GLFWwindow *p_window, unsigned int p_code_point) {
        glfw_character_mods_callback(p_window, p_code_point, 0);
    }
}