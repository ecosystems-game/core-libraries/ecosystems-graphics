#include "eco-graphics/Buffer.hpp"
#include <cassert>
#include <cstring>

namespace ecosystems::graphics {
/**
 * Returns the minimum instance size required to be compatible with devices minOffsetAlignment
 *
 * @param instanceSize The size of an instance
 * @param minOffsetAlignment The minimum required alignment, in bytes, for the offset member (eg
 * minUniformBufferOffsetAlignment)
 *
 * @return VkResult of the m_buffer mapping call
 */
    VkDeviceSize Buffer::GetAlignment(VkDeviceSize instanceSize, VkDeviceSize minOffsetAlignment) {
        if (minOffsetAlignment > 0) {
            return (instanceSize + minOffsetAlignment - 1) & ~(minOffsetAlignment - 1);
        }
        return instanceSize;
    }

    Buffer::Buffer(
            Device &p_device,
            VkDeviceSize p_instance_size,
            uint32_t p_instance_count,
            VkBufferUsageFlags p_usage_flags,
            VkMemoryPropertyFlags p_memory_property_flags,
            VkDeviceSize p_min_offset_alignment)
            : m_device{p_device},
              m_instance_count{p_instance_count},
              m_instance_size{p_instance_size},
              m_usage_flags{p_usage_flags},
              m_memory_property_flags{p_memory_property_flags} {
        m_alignment_size = GetAlignment(p_instance_size, p_min_offset_alignment);
        m_buffer_size = m_alignment_size * p_instance_count;
        p_device.CreateVkBuffer(m_buffer_size, p_usage_flags, p_memory_property_flags, m_buffer, m_memory);
    }

    Buffer::~Buffer() {
        Unmap();
        vkDestroyBuffer(m_device.get_device(), m_buffer, nullptr);
        vkFreeMemory(m_device.get_device(), m_memory, nullptr);
    }

/**
 * Map a m_memory range of this m_buffer. If successful, m_mapped points to the specified m_buffer range.
 *
 * @param size (Optional) Size of the m_memory range to map. Pass VK_WHOLE_SIZE to map the complete
 * m_buffer range.
 * @param offset (Optional) Byte offset from beginning
 *
 * @return VkResult of the m_buffer mapping call
 */
    VkResult Buffer::Map(VkDeviceSize size, VkDeviceSize offset) {
        assert(m_buffer && m_memory && "Called map on m_buffer before create");
        return vkMapMemory(m_device.get_device(), m_memory, offset, size, 0, &m_mapped);
    }

/**
 * Unmap a m_mapped m_memory range
 *
 * @note Does not return a result as vkUnmapMemory can't fail
 */
    void Buffer::Unmap() {
        if (m_mapped) {
            vkUnmapMemory(m_device.get_device(), m_memory);
            m_mapped = nullptr;
        }
    }

/**
 * Copies the specified data to the m_mapped m_buffer. Default value writes whole m_buffer range
 *
 * @param data Pointer to the data to copy
 * @param size (Optional) Size of the data to copy. Pass VK_WHOLE_SIZE to Flush the complete m_buffer
 * range.
 * @param offset (Optional) Byte offset from beginning of m_mapped region
 *
 */
    void Buffer::WriteToBuffer(void *data, VkDeviceSize size, VkDeviceSize offset) {
        assert(m_mapped && "Cannot copy to unmapped m_buffer");

        if (size == VK_WHOLE_SIZE) {
            memcpy(m_mapped, data, m_buffer_size);
        } else {
            char *memOffset = (char *)m_mapped;
            memOffset += offset;
            memcpy(memOffset, data, size);
        }
    }

/**
 * Flush a m_memory range of the m_buffer to make it visible to the device
 *
 * @note Only required for non-coherent m_memory
 *
 * @param size (Optional) Size of the m_memory range to flush. Pass VK_WHOLE_SIZE to Flush the
 * complete m_buffer range.
 * @param offset (Optional) Byte offset from beginning
 *
 * @return VkResult of the Flush call
 */
    VkResult Buffer::Flush(VkDeviceSize size, VkDeviceSize offset) {
        VkMappedMemoryRange mappedRange = {};
        mappedRange.sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
        mappedRange.memory = m_memory;
        mappedRange.offset = offset;
        mappedRange.size = size;
        return vkFlushMappedMemoryRanges(m_device.get_device(), 1, &mappedRange);
    }

/**
 * Invalidate a m_memory range of the m_buffer to make it visible to the host
 *
 * @note Only required for non-coherent m_memory
 *
 * @param size (Optional) Size of the m_memory range to invalidate. Pass VK_WHOLE_SIZE to Invalidate
 * the complete m_buffer range.
 * @param offset (Optional) Byte offset from beginning
 *
 * @return VkResult of the Invalidate call
 */
    VkResult Buffer::Invalidate(VkDeviceSize size, VkDeviceSize offset) {
        VkMappedMemoryRange mappedRange = {};
        mappedRange.sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
        mappedRange.memory = m_memory;
        mappedRange.offset = offset;
        mappedRange.size = size;
        return vkInvalidateMappedMemoryRanges(m_device.get_device(), 1, &mappedRange);
    }

/**
 * Create a m_buffer info descriptor
 *
 * @param size (Optional) Size of the m_memory range of the descriptor
 * @param offset (Optional) Byte offset from beginning
 *
 * @return VkDescriptorBufferInfo of specified offset and range
 */
    VkDescriptorBufferInfo Buffer::get_descriptor_info(VkDeviceSize size, VkDeviceSize offset) {
        return VkDescriptorBufferInfo{
                m_buffer,
                offset,
                size,
        };
    }

/**
 * Copies "m_instance_size" bytes of data to the m_mapped m_buffer at an offset of index * m_alignment_size
 *
 * @param data Pointer to the data to copy
 * @param index Used in offset calculation
 *
 */
    void Buffer::WriteToIndex(void *data, int index) {
        WriteToBuffer(data, m_instance_size, index * m_alignment_size);
    }

/**
 *  Flush the m_memory range at index * m_alignment_size of the m_buffer to make it visible to the device
 *
 * @param index Used in offset calculation
 *
 */
    VkResult Buffer::FlushIndex(int index) { return Flush(m_alignment_size, index * m_alignment_size); }

/**
 * Create a m_buffer info descriptor
 *
 * @param index Specifies the region given by index * m_alignment_size
 *
 * @return VkDescriptorBufferInfo for instance at index
 */
    VkDescriptorBufferInfo Buffer::DescriptorInfoForIndex(int index) {
        return get_descriptor_info(m_alignment_size, index * m_alignment_size);
    }

/**
 * Invalidate a m_memory range of the m_buffer to make it visible to the host
 *
 * @note Only required for non-coherent m_memory
 *
 * @param index Specifies the region to Invalidate: index * m_alignment_size
 *
 * @return VkResult of the Invalidate call
 */
    VkResult Buffer::InvalidateIndex(int index) {
        return Invalidate(m_alignment_size, index * m_alignment_size);
    }
}