#include "eco-graphics/Renderer.hpp"
#include "eco-graphics/RenderWindow.hpp"
#include "eco-graphics/Model.hpp"

#include <array>
#include <cassert>
#include <stdexcept>


namespace ecosystems::graphics {

    Renderer::Renderer(graphics::RenderWindow& p_window, graphics::Device& p_device)
        : m_window{p_window}, m_device{p_device}
    {
        RecreateVkSwapChain();
        CreateVkCommandBuffers();
    }

    Renderer::~Renderer() {
        FreeVkCommandBuffers();
    }

    void Renderer::CreateVkCommandBuffers() {
        m_command_buffers.resize(SwapChain::MAX_FRAMES_IN_FLIGHT);

        VkCommandBufferAllocateInfo allocateInfo;
        allocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        allocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        allocateInfo.commandPool = m_device.get_command_pool();
        allocateInfo.commandBufferCount = static_cast<uint32_t>(m_command_buffers.size());
        allocateInfo.pNext = nullptr;

        if(vkAllocateCommandBuffers(m_device.get_device(), &allocateInfo, m_command_buffers.data()) != VK_SUCCESS) {
            throw std::runtime_error("Failed to allocate command buffers!");
        }
    }

    void Renderer::RecreateVkSwapChain() {

        auto extent = m_window.get_extent();
        while(extent.width == 0 || extent.height == 0) {
            extent = m_window.get_extent();
            glfwWaitEvents();
        }

        vkDeviceWaitIdle(m_device.get_device());

        if(m_swap_chain == nullptr) {
            m_swap_chain = std::make_unique<SwapChain>(m_device, extent);
        } else {
            std::shared_ptr<SwapChain> oldSwapChain = std::move(m_swap_chain);
            m_swap_chain = std::make_unique<SwapChain>(m_device, extent, oldSwapChain);

            if(!oldSwapChain->CompareSwapFormats(*m_swap_chain)) {
                throw std::runtime_error("Swap chain image (or depth) format has changed!");
            }
        }
    }

    void Renderer::FreeVkCommandBuffers() {
        vkFreeCommandBuffers(
                m_device.get_device(),
                m_device.get_command_pool(),
                static_cast<float>(m_command_buffers.size()),
                m_command_buffers.data());
        m_command_buffers.clear();
    }

    VkCommandBuffer Renderer::BeginFrame() {
        assert(!m_is_frame_started && "Can't call BeginFrame while already in progress");

        auto result = m_swap_chain->AcquireNextImage(&m_current_image_index);
        if(result == VK_ERROR_OUT_OF_DATE_KHR) {
            RecreateVkSwapChain();
            return nullptr;
        }

        if(result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR) {
            throw std::runtime_error("Failed to acquire swap chain image!");
        }

        m_is_frame_started = true;
        auto commandBuffer = get_current_command_buffer();

        VkCommandBufferBeginInfo beginInfo{};
        beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        beginInfo.pNext = nullptr;

        if(vkBeginCommandBuffer(commandBuffer, &beginInfo) != VK_SUCCESS) {
            throw std::runtime_error("Failed to begin recording command m_buffer!");
        }

        return commandBuffer;
    }

    void Renderer::EndFrame() {
        assert(m_is_frame_started && "Can't end frame while frame not in progress!");
        auto commandBuffer = get_current_command_buffer();
        if(vkEndCommandBuffer(commandBuffer) != VK_SUCCESS) {
            throw std::runtime_error("Failed to record command m_buffer!");
        }

        auto result = m_swap_chain->SubmitVkCommandBuffers(&commandBuffer, &m_current_image_index);
        if(result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR || m_window.was_window_resized()) {
            m_window.reset_window_resized_flag();
            RecreateVkSwapChain();
        } else if(result != VK_SUCCESS) {
            throw std::runtime_error("Failed to present swap chain image!");
        }

        m_is_frame_started = false;
        m_current_frame_index = (m_current_frame_index + 1) % graphics::SwapChain::MAX_FRAMES_IN_FLIGHT;
    }

    void Renderer::BeginSwapChainRenderPass(VkCommandBuffer p_command_buffer) {
        assert(m_is_frame_started && "Can't Begin Render Pass while frame not in progress!");
        assert(p_command_buffer == get_current_command_buffer() && "Can't Begin Render Pass on Command Buffer from a different frame!");

        VkRenderPassBeginInfo renderPassInfo{};
        renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        renderPassInfo.renderPass = m_swap_chain->get_render_pass();
        renderPassInfo.framebuffer = m_swap_chain->get_frame_buffer(m_current_image_index);
        renderPassInfo.renderArea.offset = {0, 0};
        renderPassInfo.renderArea.extent = m_swap_chain->get_swap_chain_extent();

        std::array<VkClearValue, 2> clearValues{};
        clearValues[0].color = {{0.01f, 0.01f, 0.01f, 1.0f}};
        clearValues[1].depthStencil = {1.0f, 0};
        renderPassInfo.clearValueCount = static_cast<uint32_t>(clearValues.size());
        renderPassInfo.pClearValues = clearValues.data();

        vkCmdBeginRenderPass(p_command_buffer, &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

        VkViewport viewport{};
        viewport.x = 0.0f;
        viewport.y = 0.05;
        viewport.width = static_cast<float>(m_swap_chain->get_swap_chain_extent().width);
        viewport.height = static_cast<float>(m_swap_chain->get_swap_chain_extent().height);
        viewport.minDepth = 0.0f;
        viewport.maxDepth = 1.0f;

        VkRect2D scissor{{0,0}, m_swap_chain->get_swap_chain_extent()};
        vkCmdSetViewport(p_command_buffer, 0, 1, &viewport);
        vkCmdSetScissor(p_command_buffer, 0, 1, &scissor);
    }

    void Renderer::EndSwapChainRenderPass(VkCommandBuffer p_command_buffer) {
        assert(m_is_frame_started && "Can't End Render Pass while frame not in progress!");
        assert(p_command_buffer == get_current_command_buffer() && "Can't End Render Pass on Command Buffer from a different frame!");
        vkCmdEndRenderPass(p_command_buffer);
    }

}