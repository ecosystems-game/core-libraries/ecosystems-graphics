#include <eco-graphics/GraphicsResourceManager.hpp>
#include <thirdparty/json.hpp>
#include <fstream>


namespace ecosystems::graphics {

    void GraphicsResourceManager::LoadResourcesFromFile(const char *p_resource_file, Renderer& p_renderer) {
        try {
            std::ifstream resourceFS;
            resourceFS.exceptions(std::ifstream::failbit);
            resourceFS.open(p_resource_file, std::ifstream::binary);
            nlohmann::json resourceJson = nlohmann::json::parse(resourceFS);

            // Load Shaders
            for (auto &element: resourceJson["shaders"]) {
                std::string tag = element["tag"].get<std::string>();
                std::string t = element["type"].get<std::string>();
                ShaderType shaderType = GetShaderType(t);

                LoadShader(tag,
                           element["path"].get<std::string>(),
                           shaderType);
            }

            // Load Pipelines
            for (auto &element: resourceJson["pipelines"]) {
                std::string tag = element["tag"].get<std::string>();

                std::string fragTag = element["shaders"].value("frag", "");
                std::string vertTag = element["shaders"].value("vert", "");
                std::string tessEvalTag = element["shaders"].value("tess_eval", "");
                std::string tessCtrlTag = element["shaders"].value("tess_ctrl", "");
                std::string geomTag = element["shaders"].value("geom", "");

                ShaderCollection shaderCollection;
                if(!fragTag.empty()) shaderCollection.frag_shader = get_shader_ptr(fragTag);
                if(!vertTag.empty()) shaderCollection.vert_shader = get_shader_ptr(vertTag);
                if(!tessEvalTag.empty()) shaderCollection.tess_eval_shader = get_shader_ptr(tessEvalTag);
                if(!tessCtrlTag.empty()) shaderCollection.tess_ctrl_shader = get_shader_ptr(tessCtrlTag);
                if(!geomTag.empty()) shaderCollection.geom_shader = get_shader_ptr(geomTag);

                LoadPipeline(tag, shaderCollection, p_renderer);
            }

            // Load Materials
            for (auto &element: resourceJson["materials"]) {
                std::string tag = element["tag"].get<std::string>();
                std::string pipelineTag = element["pipeline"].get<std::string>();

                LoadMaterial(tag, get_pipeline(pipelineTag));
            }

            // Load Models
            for (auto &element: resourceJson["models"]) {
                std::string tag = element["tag"].get<std::string>();
                std::string path = element["path"].get<std::string>();
                LoadModel(tag, path.c_str());
            }

            // Load Meshes
            for (auto &element: resourceJson["meshes"]) {
                std::string tag = element["tag"].get<std::string>();
                std::string modelTag = element["model"].get<std::string>();
                std::string materialTag = element["material"].get<std::string>();

                LoadMesh(tag, get_model(modelTag), get_material(materialTag));
            }
        } catch(nlohmann::json::exception& e) {
            std::cerr << e.what() << std::endl;
            throw exceptions::GraphicsResourceException("Failed to parse graphics resource file!");
        } catch(exceptions::GraphicsResourceException& e) {
            std::cerr << e.what() << std::endl;
            throw e;
        }
    }

    void graphics::GraphicsResourceManager::LoadShader(std::string p_shader_tag, std::string p_shader_file, ShaderType p_shader_type) {
        m_shaders[p_shader_tag] = std::make_shared<Shader>(p_shader_file, "main", p_shader_type, m_device);
    }

    void GraphicsResourceManager::LoadPipeline(std::string p_pipeline_tag, ShaderCollection& p_shader_collection, Renderer& p_renderer) {
        m_pipelines[p_pipeline_tag] = std::make_unique<Pipeline>(m_device, p_shader_collection, p_renderer.get_swap_chain_render_pass());
    }

    void GraphicsResourceManager::LoadMaterial(std::string p_material_tag, Pipeline &p_pipeline) {
        m_materials[p_material_tag] = std::make_unique<Material>(p_pipeline);
    }

    void GraphicsResourceManager::LoadModel(std::string p_model_tag, const char *p_model_file) {
        m_models[p_model_tag] = Model::createModelFromFile(m_device, p_model_file);
    }

    void GraphicsResourceManager::LoadMesh(std::string p_mesh_tag, Model &p_model, Material &p_material) {
        m_meshes[p_mesh_tag] = std::make_unique<Mesh>(p_model, p_material);
    }

    graphics::Shader& GraphicsResourceManager::get_shader(std::string p_shader_tag) {
        auto it = m_shaders.find(p_shader_tag);
        if(it != m_shaders.end()) {
            return *(it->second);
        }

        throw exceptions::GraphicsResourceException("Shader resource not found!");
    }

    std::shared_ptr<graphics::Shader>& GraphicsResourceManager::get_shader_ptr(std::string p_shader_tag) {
        auto it = m_shaders.find(p_shader_tag);
        if(it != m_shaders.end()) {
            return it->second;
        }

        throw exceptions::GraphicsResourceException("Shader resource not found!");
    }

    Pipeline& GraphicsResourceManager::get_pipeline(std::string p_pipeline_tag) {
        auto it = m_pipelines.find(p_pipeline_tag);
        if(it != m_pipelines.end()) {
            return *(it->second);
        }

        throw exceptions::GraphicsResourceException("Pipeline resource not found!");
    }

    Material& GraphicsResourceManager::get_material(std::string p_mat_tag) {
        auto it = m_materials.find(p_mat_tag);
        if(it != m_materials.end()) {
            return *(it->second);
        }

        throw exceptions::GraphicsResourceException("Material resource not found!");
    }

    Model& GraphicsResourceManager::get_model(std::string p_model_tag) {
        auto it = m_models.find(p_model_tag);
        if(it->second != nullptr && it != m_models.end()) {
            return *(it->second);
        }

        throw exceptions::GraphicsResourceException("Model resource not found!");
    }

    Mesh& GraphicsResourceManager::get_mesh(std::string p_mesh_tag) {
        auto it = m_meshes.find(p_mesh_tag);
        if(it != m_meshes.end()) {
            return *(it->second);
        }

        throw exceptions::GraphicsResourceException("Mesh resource not found!");
    }
}