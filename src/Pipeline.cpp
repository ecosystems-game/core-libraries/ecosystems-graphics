#include <eco-graphics/Pipeline.hpp>
#include <eco-graphics/Model.hpp>
#include <eco-graphics/PushConstant.hpp>
#include <cassert>
#include <utility>


namespace ecosystems::graphics {
    Pipeline::Pipeline(Device &p_device,
                       ShaderCollection& p_shader_collection,
                       VkRenderPass p_render_pass)
        : m_device{p_device},
          m_shader_collection(p_shader_collection),
          m_shader_flags(GenerateStageFlags())
    {
        AddPushConstantRange(sizeof(PushConstant), m_shader_flags);

        PipelineConfigInfo pipelineConfig{};
        Pipeline::DefaultPipelineConfigInfo(pipelineConfig);
        pipelineConfig.renderPass = p_render_pass;

        CreateVkPipelineLayout();
        pipelineConfig.pipelineLayout = m_pipeline_layout;

        std::vector<VkPipelineShaderStageCreateInfo> stageInfo = CreateShaderStageInfo();
        CreateVkGraphicsPipeline(pipelineConfig, stageInfo);
    }

    Pipeline::~Pipeline() {
        vkDestroyPipeline(m_device.get_device(), m_graphics_pipeline, nullptr);
        vkDestroyPipelineLayout(m_device.get_device(), m_pipeline_layout, nullptr);
    }

    void Pipeline::CreateVkGraphicsPipeline(const PipelineConfigInfo& p_config_info, std::vector<VkPipelineShaderStageCreateInfo>& p_stages) {
        assert(p_config_info.pipelineLayout != VK_NULL_HANDLE &&
               "Cannot Create graphics pipeline:: No pipelineLayout provided to p_config_info!");
        assert(p_config_info.renderPass != VK_NULL_HANDLE &&
               "Cannot Create graphics pipeline:: No renderPass provided to p_config_info!");

        auto bindingDescriptions = p_config_info.bindingDescriptions;
        auto attributeDescriptions = p_config_info.attributeDescriptions;

        VkPipelineVertexInputStateCreateInfo vertexInputInfo{};
        vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
        vertexInputInfo.vertexBindingDescriptionCount = static_cast<uint32_t>(bindingDescriptions.size());
        vertexInputInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(attributeDescriptions.size());
        vertexInputInfo.pVertexAttributeDescriptions = attributeDescriptions.data();
        vertexInputInfo.pVertexBindingDescriptions = bindingDescriptions.data();

        VkGraphicsPipelineCreateInfo pipelineInfo {};
        pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
        pipelineInfo.stageCount = p_stages.size();
        pipelineInfo.pStages = p_stages.data();
        pipelineInfo.pVertexInputState = &vertexInputInfo;
        pipelineInfo.pInputAssemblyState = &p_config_info.inputAssemblyInfo;
        pipelineInfo.pViewportState = &p_config_info.viewportInfo;
        pipelineInfo.pRasterizationState = &p_config_info.rasterizationInfo;
        pipelineInfo.pMultisampleState = &p_config_info.multisampleInfo;
        pipelineInfo.pColorBlendState = &p_config_info.colorBlendInfo;
        pipelineInfo.pDepthStencilState = &p_config_info.depthStencilInfo;
        pipelineInfo.pDynamicState = &p_config_info.dynamicStateInfo;

        pipelineInfo.layout = p_config_info.pipelineLayout;
        pipelineInfo.renderPass = p_config_info.renderPass;
        pipelineInfo.subpass = p_config_info.subpass;

        pipelineInfo.basePipelineIndex = -1;
        pipelineInfo.basePipelineHandle = VK_NULL_HANDLE;

        if (vkCreateGraphicsPipelines(m_device.get_device(), VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, &m_graphics_pipeline) != VK_SUCCESS) {
            throw std::runtime_error("Failed to create graphics pipeline!");
        }

    }

    void Pipeline::DefaultPipelineConfigInfo(PipelineConfigInfo& p_config_info) {
        p_config_info.inputAssemblyInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
        p_config_info.inputAssemblyInfo.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
        p_config_info.inputAssemblyInfo.primitiveRestartEnable = VK_FALSE;

        p_config_info.viewportInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
        p_config_info.viewportInfo.viewportCount = 1;
        p_config_info.viewportInfo.pViewports = nullptr;
        p_config_info.viewportInfo.scissorCount = 1;
        p_config_info.viewportInfo.pScissors = nullptr;

        p_config_info.rasterizationInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
        p_config_info.rasterizationInfo.depthClampEnable = VK_FALSE;
        p_config_info.rasterizationInfo.rasterizerDiscardEnable = VK_FALSE;
        p_config_info.rasterizationInfo.polygonMode = VK_POLYGON_MODE_FILL;
        p_config_info.rasterizationInfo.lineWidth = 1.0f;
        p_config_info.rasterizationInfo.cullMode = VK_CULL_MODE_FRONT_BIT;
        p_config_info.rasterizationInfo.frontFace = VK_FRONT_FACE_CLOCKWISE;
        p_config_info.rasterizationInfo.depthBiasEnable = VK_FALSE;
        p_config_info.rasterizationInfo.depthBiasConstantFactor = 0.0f;  // Optional
        p_config_info.rasterizationInfo.depthBiasClamp = 0.0f;           // Optional
        p_config_info.rasterizationInfo.depthBiasSlopeFactor = 0.0f;     // Optional

        p_config_info.multisampleInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
        p_config_info.multisampleInfo.sampleShadingEnable = VK_FALSE;
        p_config_info.multisampleInfo.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
        p_config_info.multisampleInfo.minSampleShading = 1.0f;           // Optional
        p_config_info.multisampleInfo.pSampleMask = nullptr;             // Optional
        p_config_info.multisampleInfo.alphaToCoverageEnable = VK_FALSE;  // Optional
        p_config_info.multisampleInfo.alphaToOneEnable = VK_FALSE;       // Optional

        p_config_info.colorBlendAttachment.colorWriteMask =
                VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT |
                VK_COLOR_COMPONENT_A_BIT;
        p_config_info.colorBlendAttachment.blendEnable = VK_FALSE;
        p_config_info.colorBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_ONE;   // Optional
        p_config_info.colorBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;  // Optional
        p_config_info.colorBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD;              // Optional
        p_config_info.colorBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;   // Optional
        p_config_info.colorBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;  // Optional
        p_config_info.colorBlendAttachment.alphaBlendOp = VK_BLEND_OP_ADD;              // Optional

        p_config_info.colorBlendInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
        p_config_info.colorBlendInfo.logicOpEnable = VK_FALSE;
        p_config_info.colorBlendInfo.logicOp = VK_LOGIC_OP_COPY;  // Optional
        p_config_info.colorBlendInfo.attachmentCount = 1;
        p_config_info.colorBlendInfo.pAttachments = &p_config_info.colorBlendAttachment;
        p_config_info.colorBlendInfo.blendConstants[0] = 0.0f;  // Optional
        p_config_info.colorBlendInfo.blendConstants[1] = 0.0f;  // Optional
        p_config_info.colorBlendInfo.blendConstants[2] = 0.0f;  // Optional
        p_config_info.colorBlendInfo.blendConstants[3] = 0.0f;  // Optional

        p_config_info.depthStencilInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
        p_config_info.depthStencilInfo.depthTestEnable = VK_TRUE;
        p_config_info.depthStencilInfo.depthWriteEnable = VK_TRUE;
        p_config_info.depthStencilInfo.depthCompareOp = VK_COMPARE_OP_LESS;
        p_config_info.depthStencilInfo.depthBoundsTestEnable = VK_FALSE;
        p_config_info.depthStencilInfo.minDepthBounds = 0.0f;  // Optional
        p_config_info.depthStencilInfo.maxDepthBounds = 1.0f;  // Optional
        p_config_info.depthStencilInfo.stencilTestEnable = VK_FALSE;
        p_config_info.depthStencilInfo.front = {};  // Optional
        p_config_info.depthStencilInfo.back = {};   // Optional

        p_config_info.dynamicStateEnables = {VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_SCISSOR};
        p_config_info.dynamicStateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
        p_config_info.dynamicStateInfo.pDynamicStates = p_config_info.dynamicStateEnables.data();
        p_config_info.dynamicStateInfo.dynamicStateCount =
                static_cast<uint32_t>(p_config_info.dynamicStateEnables.size());
        p_config_info.dynamicStateInfo.flags = 0;

        p_config_info.bindingDescriptions = Model::Vertex::get_binding_descriptions();
        p_config_info.attributeDescriptions = Model::Vertex::get_attribute_descriptions();
    }

    void Pipeline::Bind(VkCommandBuffer p_command_buffer) {
        // Other pipelines to bind to: VK_BIND_POINT_COMPUTE, VK_BIND_POINT_RAY_TRACING_KHR
        vkCmdBindPipeline(p_command_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, m_graphics_pipeline);
    }

    std::vector<VkPipelineShaderStageCreateInfo> Pipeline::CreateShaderStageInfo() {
        std::vector<VkPipelineShaderStageCreateInfo> shaderStages;

        if(m_shader_collection.vert_shader != nullptr) {
            VkPipelineShaderStageCreateInfo shaderStageInfo{};
            shaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
            shaderStageInfo.stage = VK_SHADER_STAGE_VERTEX_BIT;
            shaderStageInfo.module = m_shader_collection.vert_shader->get_vk_shader_module();
            shaderStageInfo.pName = m_shader_collection.vert_shader->get_shader_name();
            shaderStages.push_back(shaderStageInfo);
        }
        if(m_shader_collection.frag_shader != nullptr) {
            VkPipelineShaderStageCreateInfo shaderStageInfo{};
            shaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
            shaderStageInfo.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
            shaderStageInfo.module = m_shader_collection.frag_shader->get_vk_shader_module();
            shaderStageInfo.pName = m_shader_collection.frag_shader->get_shader_name();
            shaderStages.push_back(shaderStageInfo);
        }
        if(m_shader_collection.tess_ctrl_shader != nullptr) {
            VkPipelineShaderStageCreateInfo shaderStageInfo{};
            shaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
            shaderStageInfo.stage = VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT;
            shaderStageInfo.module = m_shader_collection.tess_ctrl_shader->get_vk_shader_module();
            shaderStageInfo.pName = m_shader_collection.tess_ctrl_shader->get_shader_name();
            shaderStages.push_back(shaderStageInfo);
        }
        if(m_shader_collection.tess_eval_shader != nullptr) {
            VkPipelineShaderStageCreateInfo shaderStageInfo{};
            shaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
            shaderStageInfo.stage = VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT;
            shaderStageInfo.module = m_shader_collection.tess_eval_shader->get_vk_shader_module();
            shaderStageInfo.pName = m_shader_collection.tess_eval_shader->get_shader_name();
            shaderStages.push_back(shaderStageInfo);
        }
        if(m_shader_collection.geom_shader != nullptr) {
            VkPipelineShaderStageCreateInfo shaderStageInfo{};
            shaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
            shaderStageInfo.stage = VK_SHADER_STAGE_GEOMETRY_BIT;
            shaderStageInfo.module = m_shader_collection.geom_shader->get_vk_shader_module();
            shaderStageInfo.pName = m_shader_collection.geom_shader->get_shader_name();
            shaderStages.push_back(shaderStageInfo);
        }

        return shaderStages;
    }

    void Pipeline::AddPushConstantRange(size_t p_push_constant_size, VkShaderStageFlags p_stages) {
        VkPushConstantRange pushConstantRange{};
        pushConstantRange.stageFlags = p_stages;
        pushConstantRange.size = p_push_constant_size;

        m_push_constant_ranges.push_back(pushConstantRange);
    }

    void Pipeline::CreateVkPipelineLayout() {
        VkPipelineLayoutCreateInfo pipelineLayoutInfo{};
        pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
        pipelineLayoutInfo.setLayoutCount = 0;
        pipelineLayoutInfo.pSetLayouts = nullptr;
        pipelineLayoutInfo.pushConstantRangeCount = m_push_constant_ranges.size();
        pipelineLayoutInfo.pPushConstantRanges = m_push_constant_ranges.data();
        if(vkCreatePipelineLayout(m_device.get_device(), &pipelineLayoutInfo, nullptr, &m_pipeline_layout) != VK_SUCCESS) {
            throw std::runtime_error("Failed to create Pipeline Layout!");
        }
    }

    VkShaderStageFlags Pipeline::GenerateStageFlags() {
        VkShaderStageFlags stageFlags = 0;
        if(m_shader_collection.vert_shader)
            stageFlags = stageFlags | VK_SHADER_STAGE_VERTEX_BIT;
        if(m_shader_collection.frag_shader)
            stageFlags = stageFlags | VK_SHADER_STAGE_FRAGMENT_BIT;
        if(m_shader_collection.tess_eval_shader)
            stageFlags = stageFlags | VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT;
        if(m_shader_collection.tess_ctrl_shader)
            stageFlags = stageFlags | VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT;
        if(m_shader_collection.geom_shader)
            stageFlags = stageFlags | VK_SHADER_STAGE_GEOMETRY_BIT;
        return stageFlags;
    }
}