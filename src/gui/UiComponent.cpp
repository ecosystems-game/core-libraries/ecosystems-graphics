#include <eco-graphics/gui/UiComponent.hpp>


namespace ecosystems::graphics::gui {
    void UiComponent::on_glfw_error(int p_error, const char *p_description) {}
    void UiComponent::on_frame_buffer_resize(int p_width, int p_height) {}
    void UiComponent::on_char_input(unsigned int p_code_point, int p_mods) {}
    void UiComponent::on_key_up(int p_key, int p_scancode, int p_mods) {}
    void UiComponent::on_key_down(int p_key, int p_scancode, int p_mods) {}
    void UiComponent::on_click(int p_button, int p_mods, glm::dvec2 p_mouse_position) {}
    void UiComponent::on_mouse_enter(glm::dvec2 p_mouse_position) {}
    void UiComponent::on_mouse_leave(glm::dvec2 p_mouse_position) {}

    void UiComponent::on_mouse_button_down(int p_button, int p_mods, glm::dvec2 p_mouse_position) {
        m_active = false;
        if(!m_mouse_down && is_within_bounds(p_mouse_position)) {
            m_active = true;
            m_mouse_down = true;
        }
    }

    void UiComponent::on_mouse_button_up(int p_button, int p_mods, glm::dvec2 p_mouse_position) {
        if(m_mouse_down && is_within_bounds(p_mouse_position)) {
            m_active = true;
            on_click(p_button, p_mods, p_mouse_position);
        }
        m_mouse_down = false;
    }

    void UiComponent::on_mouse_move(glm::dvec2 p_mouse_position) {
        if(!m_hover && is_within_bounds(p_mouse_position)) {
            m_hover = true;
            on_mouse_enter(p_mouse_position);
        } else if (m_hover) {
            m_hover = false;
            on_mouse_leave(p_mouse_position);
        }
    }
}