#version 450

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inNormals;
layout(location = 2) in vec2 inTexCoords;

layout(location = 0) out vec2 TexCoords;

layout(push_constant) uniform Push {
    mat4 transform; // projection * view * model
    mat4 modelMatrix;
} push;

void main() {
    outColor = vec4(fragColor, 1.0);
}