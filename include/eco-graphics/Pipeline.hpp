#ifndef ECOSYSTEMS_PIPELINE_HPP
#define ECOSYSTEMS_PIPELINE_HPP

#include <memory>
#include <string>
#include <utility>
#include <vector>

#include <vulkan/vulkan.h>

#include <EcoApiModule.h>
#include <eco-graphics/util.hpp>
#include <eco-graphics/Device.hpp>
#include <eco-graphics/Shader.hpp>


namespace ecosystems::graphics {

    struct ShaderCollection {
        std::shared_ptr<Shader> vert_shader = nullptr;
        std::shared_ptr<Shader> frag_shader = nullptr;
        std::shared_ptr<Shader> tess_eval_shader = nullptr;
        std::shared_ptr<Shader> tess_ctrl_shader = nullptr;
        std::shared_ptr<Shader> geom_shader = nullptr;
    };

    struct PipelineConfigInfo {
        PipelineConfigInfo(const PipelineConfigInfo&) = delete;
        PipelineConfigInfo& operator=(const PipelineConfigInfo&) = delete;

        std::vector<VkVertexInputBindingDescription> bindingDescriptions{};
        std::vector<VkVertexInputAttributeDescription> attributeDescriptions{};

        VkPipelineViewportStateCreateInfo viewportInfo;
        VkPipelineInputAssemblyStateCreateInfo inputAssemblyInfo;
        VkPipelineRasterizationStateCreateInfo rasterizationInfo;
        VkPipelineMultisampleStateCreateInfo multisampleInfo;
        VkPipelineColorBlendAttachmentState colorBlendAttachment;
        VkPipelineColorBlendStateCreateInfo colorBlendInfo;
        VkPipelineDepthStencilStateCreateInfo depthStencilInfo;

        std::vector<VkDynamicState> dynamicStateEnables;
        VkPipelineDynamicStateCreateInfo dynamicStateInfo;
        VkPipelineLayout pipelineLayout = nullptr;
        VkRenderPass renderPass = nullptr;
        uint32_t subpass = 0;
    };

    class Pipeline {
    public:
        ECO_API Pipeline(Device& p_device,
                         ShaderCollection& p_shader_collection,
                         VkRenderPass p_render_pass);
        ECO_API virtual ~Pipeline();

        Pipeline(const Pipeline &) = delete;
        Pipeline &operator=(const Pipeline&)=delete;

        ECO_API void Bind(VkCommandBuffer p_command_buffer);
        ECO_API static void DefaultPipelineConfigInfo(PipelineConfigInfo& p_config_info);
        ECO_API VkPipeline& get_pipeline() { return m_graphics_pipeline; }
        ECO_API VkPipelineLayout& get_pipeline_layout() { return m_pipeline_layout; }

        ECO_API void AddPushConstantRange(size_t p_push_constant_size, VkShaderStageFlags p_stages);
        ECO_API VkShaderStageFlags& get_stage_flags() { return m_shader_flags; }
    private:
        std::vector<VkPipelineShaderStageCreateInfo> CreateShaderStageInfo();
        void CreateVkPipelineLayout();
        void CreateVkGraphicsPipeline(
                const PipelineConfigInfo& p_config_info,
                std::vector<VkPipelineShaderStageCreateInfo>& p_stages);

        VkShaderStageFlags GenerateStageFlags();

        Device& m_device;
        ShaderCollection m_shader_collection;
        VkShaderStageFlags m_shader_flags;
        std::vector<VkPushConstantRange> m_push_constant_ranges;

        VkPipeline m_graphics_pipeline = VK_NULL_HANDLE;
        VkPipelineLayout m_pipeline_layout = VK_NULL_HANDLE;
    };
}

#endif //ECOSYSTEMS_PIPELINE_HPP
