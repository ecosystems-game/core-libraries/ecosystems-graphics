#ifndef ECOSYSTEMS_GRAPHICS_MATERIAL_HPP
#define ECOSYSTEMS_GRAPHICS_MATERIAL_HPP

#include <array>
#include <vector>

#include <eco-graphics/Pipeline.hpp>
#include <eco-graphics/Model.hpp>


namespace ecosystems::graphics {
    class Material {
    public:
        ECO_API explicit Material(Pipeline& p_pipeline) : m_pipeline(p_pipeline) {}

        ECO_API void Bind(VkCommandBuffer p_command_buffer) {
            m_pipeline.Bind(p_command_buffer);
        }

        ECO_API Pipeline& get_pipeline() { return m_pipeline; }
    private:
        Pipeline& m_pipeline;
    };
}

#endif //ECOSYSTEMS_GRAPHICS_MATERIAL_HPP
