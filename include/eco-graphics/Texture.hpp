#ifndef ECOSYSTEMS_TEXTURE_HPP
#define ECOSYSTEMS_TEXTURE_HPP

#include "Device.hpp"


namespace ecosystems::graphics {
    class Texture {
    public:
        explicit Texture(Device& p_device) : m_device(p_device){}
        ~Texture() = default;

        Texture(const Texture&)=delete;
        Texture &operator=(const Texture &)=delete;

        static void CreateImage(Device& p_device, uint32_t p_width, uint32_t p_height, VkFormat p_format, VkImageTiling p_tiling, VkImageUsageFlags p_usage, VkMemoryPropertyFlags p_properties, VkImage& p_image, VkDeviceMemory &p_image_memory);
        static std::unique_ptr<Texture> CreateTextureFromImageFile(Device& p_device, const char* p_image_file);

        VkImage& get_texture_image() { return m_texture_image; }
        VkDeviceMemory& get_texture_image_memory() { return m_texture_image_memory; }
    private:
        Device& m_device;
        VkImage m_texture_image{};
        VkDeviceMemory m_texture_image_memory{};
    };
}

#endif //ECOSYSTEMS_TEXTURE_HPP
