#ifndef ECOSYSTEMS_BUFFER_HPP
#define ECOSYSTEMS_BUFFER_HPP

#include <vulkan/vulkan.hpp>
#include <eco-graphics/Device.hpp>
#include <EcoApiModule.h>


namespace ecosystems::graphics {
    class Buffer {
    public:
        ECO_API Buffer(Device& p_device,
            VkDeviceSize p_instance_size,
            uint32_t p_instance_count,
            VkBufferUsageFlags p_usage_flags,
            VkMemoryPropertyFlags p_memory_property_flags,
            VkDeviceSize p_min_offset_alignment = 1);
        ECO_API ~Buffer();

        Buffer(const Buffer&) = delete;
        Buffer& operator=(const Buffer&) = delete;

        ECO_API VkResult Map(VkDeviceSize size = VK_WHOLE_SIZE, VkDeviceSize offset = 0);
        ECO_API void Unmap();

        ECO_API void WriteToBuffer(void* data, VkDeviceSize size = VK_WHOLE_SIZE, VkDeviceSize offset = 0);
        ECO_API VkResult Flush(VkDeviceSize size = VK_WHOLE_SIZE, VkDeviceSize offset = 0);
        ECO_API VkDescriptorBufferInfo get_descriptor_info(VkDeviceSize size = VK_WHOLE_SIZE, VkDeviceSize offset = 0);
        ECO_API VkResult Invalidate(VkDeviceSize size = VK_WHOLE_SIZE, VkDeviceSize offset = 0);

        ECO_API void WriteToIndex(void* data, int index);
        ECO_API VkResult FlushIndex(int index);
        ECO_API VkDescriptorBufferInfo DescriptorInfoForIndex(int index);
        ECO_API VkResult InvalidateIndex(int index);

        [[nodiscard]] ECO_API VkBuffer get_buffer() const { return m_buffer; }
        [[nodiscard]] ECO_API void* get_mapped_memory() const { return m_mapped; }
        [[nodiscard]] ECO_API uint32_t get_instance_count() const { return m_instance_count; }
        [[nodiscard]] ECO_API VkDeviceSize get_instance_size() const { return m_instance_size; }
        [[nodiscard]] ECO_API VkDeviceSize get_alignment_size() const { return m_instance_size; }
        [[nodiscard]] ECO_API VkBufferUsageFlags get_usage_flags() const { return m_usage_flags; }
        [[nodiscard]] ECO_API VkMemoryPropertyFlags get_memory_property_flags() const { return m_memory_property_flags; }
        [[nodiscard]] ECO_API VkDeviceSize get_buffer_size() const { return m_buffer_size; }

    private:
        static VkDeviceSize GetAlignment(VkDeviceSize instanceSize, VkDeviceSize minOffsetAlignment);

        Device& m_device;
        void* m_mapped = nullptr;
        VkBuffer m_buffer = VK_NULL_HANDLE;
        VkDeviceMemory m_memory = VK_NULL_HANDLE;

        VkDeviceSize m_buffer_size;
        uint32_t m_instance_count;
        VkDeviceSize m_instance_size;
        VkDeviceSize m_alignment_size;
        VkBufferUsageFlags m_usage_flags;
        VkMemoryPropertyFlags m_memory_property_flags;
    };
}

#endif //ECOSYSTEMS_BUFFER_HPP
