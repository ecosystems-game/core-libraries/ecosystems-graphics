#ifndef ECOSYSTEMS_GRAPHICS_FRAMEINFO_HPP
#define ECOSYSTEMS_GRAPHICS_FRAMEINFO_HPP

#include <vulkan/vulkan.hpp>
#include <glm/glm.hpp>


namespace ecosystems::graphics {
    struct FrameInfo {
        int frame_index;
        float frame_time;
        VkCommandBuffer command_buffer;
        const glm::mat4 &projection_view;
    };
}

#endif //ECOSYSTEMS_GRAPHICS_FRAMEINFO_HPP
