#ifndef ECOSYSTEMS_DEVICE_HPP
#define ECOSYSTEMS_DEVICE_HPP

#include "EcoApiModule.h"
#include "RenderWindow.hpp"
#include <vector>
#include <optional>
#include <vulkan/vulkan.h>

namespace ecosystems::graphics {

    struct SwapChainSupportDetails {
        VkSurfaceCapabilitiesKHR capabilities;
        std::vector<VkSurfaceFormatKHR> formats;
        std::vector<VkPresentModeKHR> presentModes;
    };

    struct QueueFamilyIndices {
        std::optional<uint32_t> graphicsFamily;
        std::optional<uint32_t> presentFamily;

        bool is_complete() {
            return graphicsFamily.has_value() && presentFamily.has_value();
        }
    };

    class Device {
    public:
        ECO_API explicit Device(RenderWindow& p_render_window);
        ECO_API ~Device();

        Device(const Device &) = delete;
        Device &operator=(const Device &)=delete;
        Device(Device &&)=delete;
        Device &operator=(Device&&)=delete;

        VkDevice get_device() { return m_logical_device; }
        VkSurfaceKHR get_surface() { return m_surface; }
        VkPhysicalDevice& get_physical_device() { return m_physical_device; }
        VkQueue get_graphics_queue() { return m_graphics_queue; }
        VkQueue get_present_queue() { return m_present_queue; }
        VkCommandPool get_command_pool() { return m_command_pool; }

        SwapChainSupportDetails get_swap_chain_support() { return QueryVkSwapChainSupport(m_physical_device); }
        SwapChainSupportDetails QueryVkSwapChainSupport(VkPhysicalDevice device);
        QueueFamilyIndices FindVkQueueFamilies(VkPhysicalDevice device);

        VkFormat FindSupportedFormat(const std::vector<VkFormat> &candidates, VkImageTiling tiling, VkFormatFeatureFlags features);

        void CreateImageWithInfo(
                const VkImageCreateInfo &imageInfo,
                VkMemoryPropertyFlags properties,
                VkImage &image,
                VkDeviceMemory &imageMemory);
        void CreateVkBuffer(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties,
                            VkBuffer &buffer, VkDeviceMemory &bufferMemory);
        void CopyVkBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size);
        void CopyVkBufferToImage(VkBuffer buffer, VkImage image, uint32_t width, uint32_t height, uint32_t layerCount);
        VkCommandBuffer BeginSingleTimeCommands();
        void EndSingleTimeCommands(VkCommandBuffer commandBuffer);
        uint32_t FindMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties);
        VkPhysicalDeviceProperties& get_properties() { return m_device_properties; }
    private:
        VkPhysicalDeviceProperties m_device_properties;
        static VKAPI_ATTR VkBool32  VKAPI_CALL VkDebugCallback(VkDebugUtilsMessageSeverityFlagBitsEXT p_message_severity,
                                                               VkDebugUtilsMessageTypeFlagsEXT p_message_type, const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
                                                               void* pUserData);

        void CreateVkInstance();
        void SetupVkDebugMessenger();
        void CreateVkSurface() { m_render_window.CreateVkSurface(m_vk_instance, &m_surface); }
        void SelectVkPhysicalDevice();
        void CreateVkLogicalDevice();
        void CreateVkCommandPool();

        VkResult CreateDebugUtilsMessengerEXT(VkInstance p_instance, const VkDebugUtilsMessengerCreateInfoEXT* p_create_info, const VkAllocationCallbacks* p_allocator, VkDebugUtilsMessengerEXT* p_debug_messenger);
        void DestroyDebugUtilsMessengerEXT(VkInstance p_instance, VkDebugUtilsMessengerEXT p_debug_messenger, const VkAllocationCallbacks* p_allocator);
        void PopulateDebugMessengerCreateInfo(VkDebugUtilsMessengerCreateInfoEXT& createInfo);

        bool CheckValidationLayerSupport();
        bool IsVkDeviceSuitable(VkPhysicalDevice device);
        bool CheckVkDeviceExtensionSupport(VkPhysicalDevice device);

        [[nodiscard]] std::vector<const char*> VkGetRequiredExtensions() const;

#ifdef NDEBUG
        bool m_enable_validation_layers = false;
#else
        bool m_enable_validation_layers = true;
#endif
        VkDebugUtilsMessengerEXT m_vk_debug_messenger;
        VkSurfaceKHR m_surface;
        VkCommandPool m_command_pool;
        VkInstance m_vk_instance;
        VkPhysicalDevice m_physical_device = VK_NULL_HANDLE;
        VkDevice m_logical_device;
        VkQueue m_graphics_queue, m_present_queue;
        RenderWindow& m_render_window;

        const std::vector<const char*> m_device_extensions = {VK_KHR_SWAPCHAIN_EXTENSION_NAME};
        const std::vector<const char*> m_validation_layers = {"VK_LAYER_KHRONOS_validation"};
    };
}

#endif //ECOSYSTEMS_DEVICE_HPP
