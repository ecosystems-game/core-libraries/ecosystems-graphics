#ifndef ECOSYSTEMS_GRAPHICS_UICOMPONENT_HPP
#define ECOSYSTEMS_GRAPHICS_UICOMPONENT_HPP

#include <vulkan/vulkan.hpp>
#include <eco-graphics/gui/UiTheme.hpp>
#include <eco-graphics/Device.hpp>
#include "EcoApiModule.h"


namespace ecosystems::graphics::gui {
    class UiComponent {
    public:
        explicit UiComponent(glm::ivec2 p_position, glm::ivec2 p_size)
            : m_position(p_position), m_size(p_size) {}

        // GameLoop HookUp
        ECO_API virtual void Init(Device& p_device){}
        ECO_API virtual void Update(float p_delta_time){}
        ECO_API virtual void Render(){}

        // Component Triggers
        ECO_API virtual void on_glfw_error(int p_error, const char* p_description);
        ECO_API virtual void on_frame_buffer_resize(int p_width, int p_height);
        ECO_API virtual void on_char_input(unsigned int p_code_point, int p_mods);
        ECO_API virtual void on_key_up(int p_key, int p_scancode, int p_mods);
        ECO_API virtual void on_key_down(int p_key, int p_scancode, int p_mods);
        ECO_API virtual void on_click(int p_button, int p_mods, glm::dvec2 p_mouse_position);
        ECO_API virtual void on_mouse_enter(glm::dvec2 p_mouse_position);
        ECO_API virtual void on_mouse_leave(glm::dvec2 p_mouse_position);
        ECO_API virtual void on_mouse_button_down(int p_button, int p_mods, glm::dvec2 p_mouse_position);
        ECO_API virtual void on_mouse_button_up(int p_button, int p_mods, glm::dvec2 p_mouse_position);
        ECO_API virtual void on_mouse_move(glm::dvec2 p_mouse_position);

        // Getters and Setters
        ECO_API bool get_hover() { return m_hover; }
        ECO_API bool get_mouse_down() { return m_mouse_down; }
        ECO_API bool get_active() { return m_active; }

        ECO_API void set_position(glm::dvec2 p_position) { m_position = p_position; }
        ECO_API glm::dvec2& get_position() { return m_position; }

        ECO_API void set_size(glm::dvec2 p_size) { m_size = p_size; }
        ECO_API glm::dvec2& get_size() { return m_size; }

        ECO_API bool is_within_bounds(glm::dvec2 p_point) {
            return p_point.x >= m_position.x &&
                p_point.x <=m_position.x + m_size.x &&
                p_point.y >= m_position.y &&
                p_point.y <= m_position.y + m_size.y;
        }

        ECO_API glm::dvec2 get_localized_mouse_position(glm::dvec2 p_global_position) {
            return p_global_position - m_position;
        }
    private:
        glm::dvec2 m_position, m_size = {0.f, 0.f};

        // Component States
        bool m_hover = false, m_mouse_down = false, m_active = false;
    };
}

#endif //ECOSYSTEMS_GRAPHICS_UICOMPONENT_HPP
