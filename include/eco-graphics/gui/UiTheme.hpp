#ifndef ECOSYSTEMS_GRAPHICS_UITHEME_HPP
#define ECOSYSTEMS_GRAPHICS_UITHEME_HPP

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>


namespace ecosystems::graphics::gui {
    struct UiTheme {
        glm::vec4 background_color = {1.f, 0.f, 0.f, 1.f};
        glm::vec4 border_color = {0.f, 0.f, 0.f, 1.f};
    };

    static UiTheme ECO_UI_DARK_THEME = {
        {0.1f, 0.1f, 0.1f, 1.f}, // Background Color
        {0.0f, 0.0f, 0.0f, 1.f} // Border Color
    };
}


#endif //ECOSYSTEMS_GRAPHICS_UITHEME_HPP
