#ifndef ECOSYSTEMS_RENDERER_HPP
#define ECOSYSTEMS_RENDERER_HPP

#include <EcoApiModule.h>

#include "Device.hpp"
#include "SwapChain.hpp"
#include "RenderWindow.hpp"

#include <memory>
#include <vector>
#include <cassert>

namespace ecosystems::graphics {
    class Renderer {
    public:
        ECO_API Renderer(RenderWindow& p_window, Device& p_device);
        ECO_API ~Renderer();

        Renderer(const Renderer&) = delete;
        Renderer &operator=(const Renderer&) = delete;

        ECO_API VkCommandBuffer BeginFrame();
        ECO_API void EndFrame();

        ECO_API void BeginSwapChainRenderPass(VkCommandBuffer p_command_buffer);
        ECO_API void EndSwapChainRenderPass(VkCommandBuffer p_command_buffer);

        [[nodiscard]] ECO_API VkRenderPass get_swap_chain_render_pass() const { return m_swap_chain->get_render_pass(); }
        [[nodiscard]] ECO_API float get_aspect_ratio() const { return m_swap_chain->extent_aspect_ratio(); }
        [[nodiscard]] ECO_API bool is_frame_started() const { return m_is_frame_started; }
        [[nodiscard]] ECO_API int get_frame_index() const { return m_current_frame_index; }

        [[nodiscard]] ECO_API VkCommandBuffer get_current_command_buffer() const {
            assert(m_is_frame_started && "Cannot get command buffer when frame is not in progress!");
            return m_command_buffers[m_current_frame_index];
        }
    private:
        void CreateVkCommandBuffers();
        void FreeVkCommandBuffers();
        void RecreateVkSwapChain();

        graphics::RenderWindow& m_window;
        graphics::Device& m_device;
        std::unique_ptr<SwapChain> m_swap_chain;
        std::vector<VkCommandBuffer> m_command_buffers;

        uint32_t m_current_image_index;
        int m_current_frame_index = 0;

        bool m_is_frame_started = false;
    };
}

#endif //ECOSYSTEMS_RENDERER_HPP
