#ifndef ECOSYSTEMS_GRAPHICS_PUSHCONSTANTS_HPP
#define ECOSYSTEMS_GRAPHICS_PUSHCONSTANTS_HPP

#include <glm/glm.hpp>


namespace ecosystems::graphics {
    struct PushConstant {
        glm::mat4 transform{1.f};
        glm::mat4 modelMatrix{1.f};
    };
}

#endif //ECOSYSTEMS_GRAPHICS_PUSHCONSTANTS_HPP
