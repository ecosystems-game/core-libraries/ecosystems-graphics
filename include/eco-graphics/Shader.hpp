#ifndef ECOSYSTEMS_GRAPHICS_SHADER_HPP
#define ECOSYSTEMS_GRAPHICS_SHADER_HPP

#include <vector>
#include <EcoApiModule.h>
#include <eco-graphics/util.hpp>
#include <eco-graphics/Device.hpp>
#include <vulkan/vulkan.hpp>


namespace ecosystems::graphics {
    enum ShaderType {
        Fragment=0,
        Geometry,
        Tesselation_Ctrl,
        Tesselation_Eval,
        Vertex
    };

    class Shader {
    public:
        ECO_API Shader(std::string& p_shader_file,
                       const char* p_shader_name,
                       ShaderType p_shader_type,
                       Device& p_device);
        ECO_API ~Shader();

        Shader(const Shader &) = delete;
        Shader &operator=(const Shader&)=delete;

        ECO_API ShaderType get_shader_type() { return m_shader_type; }
        ECO_API VkShaderModule& get_vk_shader_module() { return m_vk_shader_module; }
        ECO_API const char* get_shader_file() { return m_shader_file.c_str(); }
        ECO_API const char* get_shader_name() { return m_shader_name; }
    private:
        Device& m_device;
        std::string m_shader_file;
        const char* m_shader_name;
        ShaderType m_shader_type;
        VkShaderModule m_vk_shader_module;

        void CreateVkShaderModule(const std::vector<char>& code, VkShaderModule* p_shader_module);
    };
}

#endif //ECOSYSTEMS_GRAPHICS_SHADER_HPP
