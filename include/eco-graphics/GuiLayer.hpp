#ifndef ECOSYSTEMS_GRAPHICS_GUILAYER_HPP
#define ECOSYSTEMS_GRAPHICS_GUILAYER_HPP

#include <utility>
#include <vector>

#include <GLFW/glfw3.h>
#include <vulkan/vulkan.hpp>

#include <eco-graphics/gui/UiComponent.hpp>
#include <eco-graphics/gui/UiTheme.hpp>

#include "EcoApiModule.h"


namespace ecosystems::graphics {
    class GuiLayer {
    public:
        ECO_API explicit GuiLayer(
                Device& p_device,
                VkRenderPass p_render_pass,
                gui::UiTheme& p_ui_theme
        ) : m_ui_theme(p_ui_theme) {}

        ECO_API virtual void Init(Device& p_device);
        ECO_API virtual void Update(float p_delta_time);
        ECO_API virtual void Render(VkCommandBuffer& p_command_buffer);

        // GLFW Input Callbacks.
        ECO_API void glfw_error_callback(int error, const char *description);
        ECO_API void glfw_keypress_callback(GLFWwindow *p_window, int p_key, int p_scancode, int p_action, int p_mods);
        ECO_API void glfw_cursor_position_callback(GLFWwindow *p_window, double p_xpos, double p_ypos);
        ECO_API void glfw_mouse_button_callback(GLFWwindow *p_window, int p_button, int p_action, int p_mods);
        ECO_API void glfw_frame_buffer_size_callback(GLFWwindow *window, int p_width, int p_height);
        ECO_API void glfw_character_mods_callback(GLFWwindow *p_window, unsigned int p_code_point, int p_mods);
        ECO_API void glfw_character_callback(GLFWwindow *p_window, unsigned int p_code_point);
    private:
        std::vector<std::unique_ptr<gui::UiComponent>> m_ui_components;
        gui::UiTheme& m_ui_theme;


    };
}
#endif //ECOSYSTEMS_GRAPHICS_GUILAYER_HPP
